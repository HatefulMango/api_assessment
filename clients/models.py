from django.db import models
from django.utils import timezone

class Client(models.Model):
    client_name = models.CharField(max_length=100)
    client_address = models.CharField(max_length=200)
    date_created = models.DateTimeField('date created', default=timezone.now())
    date_updated = models.DateTimeField('date updated', default=timezone.now())
    client_deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ['date_created']