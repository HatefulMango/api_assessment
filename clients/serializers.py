from rest_framework import serializers
from . models import Client

class clientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ['id', 'client_name', 'client_address', 'date_created', 'date_updated' , 'client_deleted']
        extra_kwargs = {
            'date_updated': {'read_only': True},
            'date_created': {'read_only': True}
        }
