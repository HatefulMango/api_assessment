from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from django.utils import timezone

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions

from . models import Client
from . serializers import clientSerializer

class Clients(APIView):

    permission_classes = [permissions.IsAuthenticated]

    def get_client(self, pk):
        try:
            return Client.objects.get(pk = pk)
        except Client.DoesNotExist:
            raise Http404
        #Checks if the requested client object exists
    
    
    def get(self, request, pk = None):
        if pk:
            clients = self.get_client(pk)
            multiple = False
        else:
            clients = Client.objects.filter(client_deleted = False)
            multiple =True
        #Checks if the get is for a single client or a list of clients
        
        serializer = clientSerializer(clients, many = multiple)
        return Response(serializer.data)

    def post(self, request):
        serializer = clientSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            response = Response(serializer.data, status = status.HTTP_201_CREATED)
        else:
            response = Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        
        return response

    def put(self, request, pk):
        client = self.get_client(pk)
        client.date_updated = timezone.now()

        serializer = clientSerializer(client, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            response = Response(serializer.data)
        else:
            response = Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        
        return response

    def delete(self, request, pk):
        try:
            client = self.get_client(pk)
            client.client_deleted = True
            client.save()

            #Client.objects.filter(pk = pk).update(client_deleted = True)
            #Updates the client_deleted field

            response = Response({'message':"Client successfully deleted"}, status=status.HTTP_200_OK)
        except Exception as E:
            response = Response({'message':str(E)})
        
        return response
