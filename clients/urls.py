from django.urls import path, include
from . import views

urlpatterns = [
    path('clients', views.Clients.as_view(), name='client_list'),
    path('clients/<pk>', views.Clients.as_view(), name='client_detail'),
]
