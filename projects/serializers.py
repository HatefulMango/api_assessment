from rest_framework import serializers
from . models import Project

class projectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = ['id', 'client_id', 'project_desc', 'date_created', 'date_updated', 'project_deleted']
        extra_kwargs = {
            'date_updated': {'read_only': True},
            'date_created': {'read_only': True}
        }