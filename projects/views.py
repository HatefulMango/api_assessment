from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from django.utils import timezone

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions

from . models import Project
from . serializers import projectSerializer

from rest_framework import routers


# Create your views here.

class Projects(APIView):

    permission_classes = [permissions.IsAuthenticated]

    def get_project(self, pk):
        try:
            return Project.objects.get(pk = pk)
        except Project.DoesNotExist:
            raise Http404
        #Checks if the requested project object exists

    def get(self, request, pk = None):

        if pk:
            projects = self.get_project(pk)
            multiple = False
        else:
            projects = Project.objects.filter(project_deleted = False)
            multiple = True
        #Checks if the get is for a single project or a list of projects
        
        serializer = projectSerializer(projects, many = multiple)
        return Response(serializer.data)

    def post(self, request):
        serializer = projectSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            response = Response(serializer.data, status = status.HTTP_201_CREATED)
        else:
            response = Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

        return response

    def put(self, request, pk):
        project = self.get_project(pk)
        project.date_updated = timezone.now()

        serializer = projectSerializer(project, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            response = Response(serializer.data)
        else:
            response = Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        
        return response

    def delete(self, request, pk):
        try:
            project = self.get_project(pk)
            project.project_deleted = True
            project.save()
            
            #Project.objects.filter(pk = pk).update(project_deleted=True)
            #Updates the client_deleted field

            response = Response({'message':"Project successfully deleted"}, status=status.HTTP_204_NO_CONTENT)
        except Exception as E:
            response = Response({'message':str(E)})
        
        return response
