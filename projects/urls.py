from django.urls import path, include
from . import views

urlpatterns = [
    path('projects', views.Projects.as_view(), name='project_list'),
    path('projects/<pk>', views.Projects.as_view(), name='project_detail'),
]