from django.db import models
from clients.models import Client
from django.utils import timezone

class Project(models.Model):
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    project_desc = models.TextField()
    date_created = models.DateTimeField('date created', default=timezone.now())
    date_updated = models.DateTimeField('date updated', default=timezone.now())
    project_deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ['date_created']